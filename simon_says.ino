/*
 * TODO: Something smarter for blink and on/off methods
 *       so we can have a veriable amount of colors to 
 *       adjust the difficulty
 */

const int r_out = 8;
const int g_out = 9;
const int b_out = 10;

const int r_in = 2;
const int g_in = 3;
const int b_in = 4;

const int wait = 200;
const int elems = 3;
int stage;
int sequence[elems];
int recorded_input[elems];

void setup() {
  
  // Set up serial and wait for it to be ready
  Serial.begin(9600);
  while(!Serial){
  ;
  }

  set_random_seed();
  
  // Building a sequence of X elements
  buildSequence(elems);
  
  delay(5000);
  stage = 1;
  pinMode(r_out, OUTPUT);
  pinMode(g_out, OUTPUT);
  pinMode(b_out, OUTPUT);

  pinMode(r_in, INPUT);
  pinMode(g_in, INPUT);
  pinMode(b_in, INPUT);

  //test(); 
}

void loop() {
  begin_stage();
  show_sequence();
  
  record_input();
  if(check_input()){
    Serial.println("Recorded sequence is OK");
    stage++;
  }else{
    Serial.println("Recorded sequence has failed");
    Serial.println("GAME OVER");
    while(true){
      delay(10000);
    }
  }
  
  if(stage > elems){
    Serial.println("The end - You won!");
    while(true){
      ;
    }
  }
}

// Test to see if all LEDs work as they should
void test(){

  for(int i = 0;i<5;i++){
    rBlink();
    gBlink();
    bBlink();
  }

  
  for(int i = 0;i<5;i++){
  allOn();
  delay(500);
  allOff();
  delay(500);  
  }

}

void rBlink(){
  rOn();
  delay(wait);
  rOff();
  delay(wait);
}

void gBlink(){
  gOn();
  delay(wait);
  gOff();
  delay(wait);
}

void bBlink(){
  bOn();
  delay(wait);
  bOff();
  delay(wait);
}

void allBlink(){  
  allOn();
  delay(wait);
  allOff();
}


void rOn(){
  digitalWrite(r_out, HIGH);
}

void gOn(){
  digitalWrite(g_out, HIGH);
  
}

void bOn(){
  digitalWrite(b_out, HIGH);
  
}

void rOff(){
  digitalWrite(r_out, LOW);
}

void gOff(){
  digitalWrite(g_out, LOW);
}

void bOff(){
  digitalWrite(b_out, LOW);  
}


void allOff(){
  rOff();
  gOff();
  bOff();
}

void allOn(){
  rOn();
  gOn();
  bOn();
}

void buildSequence(int num_elems){
  int pins[] = {r_out, g_out, b_out};

  for(int i = 0;i<=2;i++){
    Serial.print("Pin ");
    Serial.print(i);
    Serial.print(" = ");
    Serial.println(pins[i]);
  }

  for(int i = 0;i<num_elems;i++){
    Serial.print("Starting at seq ");
    Serial.println(i);

    int rnd = random(0,3);
    Serial.print("Random is: ");
    Serial.println(rnd);

    int pin_value = pins[rnd];
    Serial.print("Pin value is: ");
    Serial.println(pin_value);

    while(pin_value == sequence[i-1]){
      rnd = random(0,3);
      pin_value = pins[rnd];
    }

    sequence[i] = pin_value;
    Serial.print("Confirm pin_value/sequence: ");
    Serial.print(pin_value);
    Serial.print(" = ");
    Serial.println(sequence[i]);
    Serial.println();
  }


  for(int i = 0;i < elems;i++){
    Serial.print(i);
    Serial.print(" is ");
    Serial.println(sequence[i]);
  }


  Serial.println();
  
}

void record_input(){
  int r_in_status = 0;
  int g_in_status = 0;
  int b_in_status = 0;

  int clicks = 0;

  Serial.print("Waiting for ");
  Serial.print(stage);
  Serial.print(" inputs...");
  Serial.println();
  
  while(clicks < stage){
    r_in_status = digitalRead(r_in);
    g_in_status = digitalRead(g_in);
    b_in_status = digitalRead(b_in);

    if(r_in_status != 0){
      Serial.print("Red is pressed and stored as click #");
      Serial.println(clicks);
      recorded_input[clicks] = r_out;
      clicks++;
      while(r_in_status != 0){
        r_in_status = digitalRead(r_in);
      }
      Serial.println("Red is released");
      Serial.println("");
      delay(100);
    }

    if(g_in_status != 0){
      Serial.print("Green is pressed and stored as click #");
      Serial.println(clicks);
      recorded_input[clicks] = g_out;
      clicks++;
      while(g_in_status != 0){
        g_in_status = digitalRead(g_in);
      }
      Serial.println("Green is released");
      Serial.println("");
      delay(100);
    }

    if(b_in_status != 0){
      Serial.print("Blue is pressed and stored as click #");
      Serial.println(clicks);
      recorded_input[clicks] = b_out;
      clicks++;
      while(b_in_status != 0){
        b_in_status = digitalRead(b_in);
      }
      Serial.println("Blue is released");
      Serial.println("");
      delay(100);
    }
  }

  Serial.println("Sequence recorded...");
  Serial.println("");
}

void show_sequence(){
  
  Serial.println("Starting sequence");

  for(int i = 0;i<stage;i++){
    Serial.print("Blinking on port ");
    Serial.println(sequence[i]);

    switch(sequence[i]){
      case r_out:
        rBlink();
        break;

      case g_out:
        gBlink();
        break;

      case b_out:
        bBlink();
        break;
    }
  }
}


void begin_stage(){
  Serial.print("Starting stage ");
  Serial.println(stage);

  for(int i = 0;i<3;i++){
    allOff();
    delay(wait);
    allOn();
    delay(wait);
    allOff();
    delay(wait);
  }

  delay(wait);
  delay(wait);
  
}

void set_random_seed(){
  int seed = analogRead(0);
  Serial.print("Using seed: ");
  Serial.println(seed);
  Serial.println("");
  randomSeed(seed);
}

bool check_input(){
  for(int i = 0;i < stage; i ++){

    Serial.print("Checking input #");
    Serial.println(i);
    
    Serial.print("Recorded:");
    Serial.println(recorded_input[i]);
    
    Serial.print("Actual:");
    Serial.println(sequence[i]);
    Serial.println("");
    if(recorded_input[i] != sequence[i]){
      return false;
    }
    
  }

  return true;
}

